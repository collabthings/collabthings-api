console.log("Start running CTApp");
import CTApp from "./modules/app";
console.log("imported CTApp");
import CTApi from "./modules/api";


let ctapi: CTApi;
let ctapp: CTApp = new CTApp();

console.log( "Running CTApp");

ctapp.init().then(() => {
    ctapi = ctapp.getApi();

    ctapi.start();

    console.log( "CTApp created" );
    //ctapi.stop();
} ).catch(( err: string ) => {
	console.log("Run error")
    if ( err ) {
        console.log( "Run creation error " + err );
        process.exit( 1 );
    }
} );

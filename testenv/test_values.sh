#!/bin/bash

export SSB_TEST001_WHOAMI=$(jq -r .userid testenv/info/001.status)
export SSB_TEST001_ADDRESS=$(jq -r .address testenv/info/001.status)
export SSB_TEST002_WHOAMI=$(jq -r .userid testenv/info/002.status)
export SSB_TEST002_ADDRESS=$(jq -r .address testenv/info/002.status)

echo SSB_TEST001_WHOAMI ${SSB_TEST001_WHOAMI}
echo SSB_TEST002_WHOAMI ${SSB_TEST002_WHOAMI}

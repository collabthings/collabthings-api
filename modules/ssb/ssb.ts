console.log("Creating CTssb");

import * as common from '../common';
const pull = require( 'pull-stream' );

import * as fs from 'fs';
import * as path from 'path'
//var ssbServer = require( 'ssb-server' )
var ssbKeys = require( 'ssb-keys' )

const makeConfig = require( 'ssb-config/inject' )
const SecretStack = require('secret-stack')
const caps = require('ssb-caps')

var network = process.env.ssb_appname || "ssb";

var home = process.env.HOME || "tmp";
var config : any = makeConfig( network , { path: home + "/." + network })
config.keys = ssbKeys.loadOrCreateSync( config.path + "/secret" );

var stack = SecretStack({ appKey: caps.shs })
    .use(require('ssb-master'))
    .use(require('ssb-db2'))
    .use(require('ssb-db2/compat')) // include all compatibility plugins
	.use(require('ssb-onion'))
    //.use(require('ssb-unix-socket'))    
    //.use( require( 'ssb-backlinks' ) )
    //.use( require( 'ssb-query' ) )
    //.use( require( 'ssb-tribes2' ) )
    .use( require( 'ssb-lan' ) )
    .use( require( 'ssb-ebt' ) )
	.use( require( 'ssb-friends' ) )
	.use( require( 'ssb-replication-scheduler' ) )
    .use(require('ssb-conn'))
    //.use( require( 'ssb-backlinks' ) )
    //.use( require( 'ssb-about' ) )
    //.use( require( 'ssb-contacts' ) )
    //.use( require( 'ssb-links' ) )
    //.use( require( 'ssb-identities' ) )
    .use( require( 'ssb-blobs' ) )
    .use( require( 'ssb-plugins' ) )
    .use( require( '../../plugins/lists' ) )

var sbot = stack.call(null, config)

const { where, and, type, author, toCallback } = require('ssb-db2/operators');

function ssblog( s: string ) {
    console.log( "SSB : " + s );
}

function checkFailedProgress( a: any ) {
    if ( a && a.index == -1 ) {
        return true;
    }
}

function checkShouldWaitProgress( a: any ) {
    if ( a && a.current != -1 && a.current != a.target ) {
        return true;
    }
}

export default class CTSsb {
    appname: string;
    ssbpath: string;

    //sbot: any;
    
    indexfailedcount: number = 0;

    whoami: string;

    constructor() {        
        //ssblog( "home: " + home + " ssb_appname:" + config.appname + " ssbpath:" + config.path );
        //ssblog( "config:" + JSON.stringify( config ) );
    }

    addPlugin( p: any ) {
		console.log("WARNING addPlugin " + p);
     	//   ssbsbot.use( p );
    }

    getSbot(): any {
        return sbot;
    }

    async startServer() {
        var self: CTSsb = this;

		/*
        ssblog( "CTSsb Writing manifest" );

        var manifest = sbot.getManifest()
        fs.writeFileSync(
            path.join( config.path, 'manifest.json' ), // ~/.ssb/manifest.json
            JSON.stringify( manifest )
        )
		*/
		
		/*
		*/
		sbot.db.onMsgAdded((event : any) => {
			console.log("event " + JSON.stringify(event, null, 4));
		});

		ssblog( "Starting conn" );
		sbot.conn.start()

        ssblog( "CTSsb server started" );
        return new Promise<String>(( resolve, reject ) => {
            self.whenServerReady( resolve, reject );
        } );
    }

    whenServerReady( resolve: Function, reject: Function ) {
        var self: CTSsb = this;

        self.delay( 2000 ).then(() => {
            var progress: any = sbot.db.getStatus().value;
            ssblog( "CTSsb server progress :" + JSON.stringify( progress ) );

            var failed: boolean = false;
            var shouldwait: boolean = false;

            ssblog( "indexes.current " + progress.indexes.current + " failedcount:" + self.indexfailedcount );

            if ( checkFailedProgress( progress.indexes ) ) failed = true;
            if ( checkFailedProgress( progress.ooo ) ) failed = true;
            //            if ( checkFailedProgress( progress.ebt ) ) failed = true;

            if ( checkShouldWaitProgress( progress.indexes ) ) shouldwait = true;
            if ( checkShouldWaitProgress( progress.ooo ) ) shouldwait = true;
            //            if ( checkShouldWaitProgress( progress.ebt ) ) shouldwait = true;

            ssblog( "shouldwait: " + shouldwait + " failed:" + failed );

            if ( shouldwait ) {
                self.whenServerReady( resolve, reject );
            } else if ( failed ) {
                self.indexfailedcount++;
                if ( self.indexfailedcount > 5 ) {
                    ssblog( "enough waiting..." );
                    ssblog( "publish started messages" );
                    self.indexfailedcount = 0;
                    resolve();
                } else {
                    self.whenServerReady( resolve, reject );
                }
            } else {
                ssblog( "Done?" );
                resolve();
            }
            
            sbot.db.create({ content: { type: 'post', text: 'hello!' } }, (err : string, msg : string) => {
  				// A new message is now published on the log, with the contents above.
  				console.log(msg)
  			});
        } );
    }

    async init(): Promise<String> {
        var self: CTSsb = this;

		await this.startServer();
		
        ssblog( "************** CTSSb init" );

        ssblog( "sbot init initUserID " + await self.initUserID());

        ssblog( "sbot names " + Object.getOwnPropertyNames( sbot ) );
        ssblog( "sbot friends names " + Object.getOwnPropertyNames( sbot.friends ) );
        //ssblog( "sbot about names " + Object.getOwnPropertyNames( sbot.about ) );
        //ssblog( "sbot test names " + Object.getOwnPropertyNames( sbot.test ) );

        ssblog( "CTSsb Success sbot " + sbot );
        
        return "SUCCESS";
    }

    public async initUserID() {
        var self: CTSsb = this;

        return new Promise<String>(( resolve, reject ) => {
			console.log("sbot " + Object.getOwnPropertyNames(sbot));
			console.log("sbot id " + sbot.id);
			console.log("sbot " + Object.getOwnPropertyNames(sbot.db));
			//console.log("sbot " + Object.getOwnPropertyNames(sbot.about));
			console.log("sbot " + sbot.getAddress());

			self.whoami = sbot.id;
			resolve(self.whoami);
        } );
    }

	public async peers(): Promise<any> {
		return new Promise(( resolve, reject ) => {
			console.log("dbPeers " + sbot.conn.dbPeers());
			sbot.conn.peers(false, (data : any) => {
				if (!data) {
					reject("PEERS FAILED");
				}
						
				console.log("peers data " + JSON.stringify(data));
				resolve(data)
			});
		});		
	}
	
    public async follow( author: string ): Promise<string> {
        ssblog( "following " + author );

		return new Promise(( resolve, reject ) => {
			/*
			var msg : string = sbot.friends.follow( author, {}, toCallback((err : string, response : string) => {
				if(err) {
					reject(err);
				}
				resolve(response);
			})
			)
			*/

			
            sbot.db.create( { content: { type: 'contact', contact: author, following: true } }, 
            				( err: string, msg: string ) => {
                if ( err ) {
                    ssblog( "ERROR follow " + err );
                    reject( err );
                } else {
                    ssblog( "flollow added " + JSON.stringify( msg ) );
                    resolve( msg );
                }
            } );

		});
    }

    public async publish( content: any ): Promise<string> {
        ssblog( "publish " + JSON.stringify( content ) );
        var msg = {
        	content: JSON.parse( JSON.stringify( content ) )
        }

        return new Promise(( resolve, reject ) => {
			ssblog( "publish in promise " + JSON.stringify( msg ) );
            sbot.db.create( msg, function( err: string, msg: string ) {
                if ( err ) {
                    ssblog( "ERROR publish " + err );
                    reject( err );
                } else {
                    ssblog( "messageAdded " + JSON.stringify( msg ) );
                    resolve( msg );
                }
            } );
        } );
    }

    public getMessagesByType( stype: string ): Promise<String> {
        var self: any = this;

		return new Promise(( resolve, reject ) => {
	        if ( sbot.collabthingsList ) {
	            sbot.collabthingsList.get(( err: string, state: any ) => {
	                if ( state && state != 'false' ) {
	                    ssblog( "sbot test state " + JSON.stringify( state ) );
	                    resolve("TODO") // TODO
	                }
	            } );
	        } else {
	            ssblog("sbot.collabthings_list not defined");
	            reject("sbot.collabthings_list not defined")
	        }
        });
    }

    public createFeedStreamByType( type: string ) : Promise<String> {
        return this.getMessagesByType( type );
    };

    public createFeedStreamByCTType( type: string ) : Promise<String> {
        return this.getMessagesByType( "collabthings-" + type);
    }

    public async addMessage( content: common.CTMessageContent, type: string ): Promise<string> {
        content.type = "collabthings-" + type;
        content.module = type;
		return await this.publish(content);
    }

    async delay( ms: number ) {
        return new Promise( resolve => setTimeout( resolve, ms ) );
    }

    public getUserID(): string {
        if ( this.whoami ) {
            return this.whoami;
        } else {
            return "";
        }
    }

    stop() {
        ssblog( "ssb stopping" );
        sbot.close();
    }
}
console.log("Creating CTApp");

import CTApi from './api';
console.log("CTApp - imported api");

import CTSsb from './ssb/ssb';
console.log("CTApp - imported ssb");

console.log("exporting CTApp");

export default class CTApp {
    ssb: CTSsb = new CTSsb();
    api: CTApi = new CTApi();

    async init() {
        var self = this;

        console.log( "Setting up CTApp" );

        await this.ssb.init();
        await self.initAfterSsb();
    }

    async initAfterSsb() {
        console.log( "CTSsb initialized" );

        try {
          this.api.init( this.ssb );
        } catch(e) {
            console.log( "CTApp error " + e );
            throw e;
        };

        return this;
    }

    getApi(): CTApi {
        return this.api;
    }

    getSsb(): CTSsb {
        return this.ssb;
    }

    async stop() {
        await this.api.stop();
        await this.ssb.stop();
    }
}

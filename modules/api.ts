import express from 'express';
import path from 'path';
import * as bodyParser from 'body-parser';

import CTSsb from './ssb/ssb';

import { CTApps, CTAppInfo } from './apps';
import { Server } from 'net';
import { CTMessageContent, CTInfo } from './common';
import { ListsApi } from './ssb/lists';
import { UsersApi } from './ssb/users';

const {where, and, type, toCallback} = require('ssb-db2/operators')

var PORT: number = 14881;

class CTApi {
    ssb: CTSsb;

    exp: express.Application;
    expserver: Server;

    lists: ListsApi;
    users: UsersApi;

    apps: CTApps;

    init( nssb: CTSsb ) {
        this.ssb = nssb;
        
        this.apps = new CTApps( this );
        this.lists = new ListsApi( this.ssb );
        this.addApp( this.lists.getAppInfo() );
        this.lists.init();

        this.users = new UsersApi( this.ssb );
        this.addApp( this.users.getAppInfo() );

        this.users.addListener(( contact: string, following: boolean ) => {
            this.apps.following( contact, following );
        } );

        this.users.init();
    }

    start() {
        this.exp = express();
        this.initExp();

        var port: number;

        console.log( "CT_API_PORT " + process.env.CT_API_PORT );
        if ( process.env.CT_API_PORT ) {
            port = +process.env.CT_API_PORT;
        } else {
            port = PORT;
        }

        this.expserver = this.exp.listen( port );
        console.log( "Listening to port " + port );
    }

    getLists(): ListsApi {
        return this.lists;
    }

    getUsers(): UsersApi {
        return this.users;
    }

    info() {
        var info = new CTInfo();
        return info;
    }

    addApp( appinfo: CTAppInfo ) {
        this.apps.addApp( appinfo );
    }

    appsList() {
        return this.apps.getList();
    }

    async addMessage( content: CTMessageContent, type: string) : Promise<String> {
        var msg : string = await this.ssb.addMessage( content, type );
        return msg
    }

    stop() {
        console.log("stopping API");
        if ( this.expserver ) {
            this.expserver.close();
        }

        return "stop done"
    }

    getInfo(): any {
        var selfinfo: { [key: string]: string } = {};

		if (this.ssb.getUserID()) {
        	selfinfo['userid'] = this.ssb.getUserID();
        }

		if (this.ssb.getSbot().getAddress()) {
        	selfinfo['address'] = this.ssb.getSbot().getAddress();
        }

        return selfinfo;
    }

    initExp() {
        var self: CTApi = this;

        var urlencodedParser = bodyParser.urlencoded( {
            extended: false
        } );

        var staticdir: string = path.join( __dirname, '../static' );
        console.log( "Serve static in " + staticdir );

        this.exp.use( "/static", express.static( staticdir, {
            setHeaders: function( res, path ) {
                console.log( "request " + path );
                if ( path.indexOf( "download" ) !== -1 ) {
                    res.attachment( path )
                }
            },
        } ) );

        this.exp.get( "/info", function( req, res ) {
            res.send( JSON.stringify( self.getInfo() ) );
        } );

		this.exp.get( "/allposts", function( req, res ) {
			self.ssb.getSbot().db.query(
			  where(
		      	type('post')
			  ),
			  toCallback((err : any, msgs : any) => {
			    console.log("posts " + JSON.stringify(msgs));
			  })
			)
            res.send( JSON.stringify( self.getInfo() ) );
        } );

        this.apps.initStatic( this.exp );

        this.exp.use( bodyParser.json() );

        this.exp.get( "/get", urlencodedParser, function( req, res ) {
            console.log( "\"get\" called " + JSON.stringify( req.body ) );
            var content: CTMessageContent = req.body;
            var type = "undefined"
            self.ssb.getMessagesByType(type).then((msg) => {
	            console.log( "message added " + JSON.stringify( msg ) );
	            res.send( JSON.stringify( msg ) );
            });
        } );



        this.exp.get( "/apps", function( req, res ) {
            res.send( JSON.stringify( self.appsList() ) );
        } );

        this.exp.get( "/info", function( req, res ) {
            console.log( "\"info\" called" );
            res.send( JSON.stringify( self.info() ) );
        } );

        this.exp.get( "/stop", function( req, res ) {
            console.log( "\"stop\" called" );
            res.send( JSON.stringify( self.stop() ) );
        } );

        this.apps.initApi( this.exp );
    }
}

export default CTApi;


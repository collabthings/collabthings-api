#!/bin/bash

if [ -z "$(docker ps | grep testenv_ssb001_1)" ]; then
	echo should run
	NO_STOP=true bash buildtest.sh
fi

if tsc; then

	docker-compose -f testenv/docker-compose.yml logs -f&

	export ssb_appname=ssb-test
	export HOME=$(pwd)/testenv/users/001 
	echo HOME ${HOME}

	. testenv/test_values.sh

	npm run test
	sleep 2

	kill %1
else
	exit 1;
fi

#!/bin/bash

set -e 

echo COLLABTHINGS-API BUILD

c=$(dirname $0)
echo current_dir $c

export PATH=$PATH:./node_modules/.bin

if [ ! -d node_modules ]; then
	npm install
fi

if tsc; then 
    DOCKER_BUILDKIT=1 BUILDKIT_PROGRESS=plain docker build . -t ct-api-base
	
	bash testenv/start.sh
	
	#cp *.json dist/
	#tsc
	
	export ssb_appname=ssb-test
	export HOME=$(pwd)/testenv/users/001 
	echo HOME ${HOME}

	export TEST_HOSTPORT1=localhost:14001
	export TEST_HOSTPORT2=localhost:14002
	export TEST_HOSTPORT3=localhost:14003
	
	. $c/testenv/test_values.sh

	npm run test

	echo tests done. 
	if [ -z "${NO_STOP}" ]; then
		echo calling stop.
		bash testenv/stop.sh
	fi
else
	exit 1;
fi

echo DONE

FROM node:19

ENV PATH "$PATH:./node_modules/.bin"
EXPOSE 8008
EXPOSE 14881

WORKDIR /code

RUN apt -y install python3

#RUN curl -L https://raw.githubusercontent.com/npm/self-installer/master/install.js | node

RUN echo update 20200408_1100

RUN npm install types @types/express express body-parser serve-static

RUN npm install leveldown
RUN npm install ssb-db2
RUN npm install ssb-feed ssb-keys
RUN npm install typescript leveldown
RUN npm install ssb-links

ADD package.json package.json
RUN npm install
RUN echo hello2
RUN cat package.json

ADD modules modules
ADD plugins plugins
ADD *.ts /code/
ADD *.json /code/
#RUN mkdir /root/.ssb

RUN ./node_modules/.bin/tsc

ENTRYPOINT node run.js

